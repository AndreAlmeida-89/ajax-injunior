
function postaMensagem(){
    let nome = document.getElementById("nome").value;
    let mensagem = document.getElementById("mensagem").value;
    
    let fetchBody = {"message":{}};
    fetchBody.message.name = nome;
    fetchBody.message.message = mensagem;
    
    const fetchConfig = {
        method: "POST",
        headers: {"Content-Type": "application/JSON"},
        body: JSON.stringify(fetchBody)
    }

    fetch("https://treinamentoajax.herokuapp.com/messages", fetchConfig)
    .then(console.log)

    document.getElementById("nome").value = "";
    document.getElementById("mensagem").value = "";
}

function obterMensagens(){
    fetch("https://treinamentoajax.herokuapp.com/messages")
    .then(response => response.json())
    .then(response => {
        for (i in response){
            let div = document.createElement("div");
            let h1 = document.createElement("h1");
            let p =  document.createElement("p");

            h1.innerHTML = response[i].name;
            p.innerHTML = response[i].message;
            
            div.appendChild(h1);
            div.appendChild(p);
            div.id = response[i].id;

            document.getElementById("chat").appendChild(div);
        }
    });

}

//DELETE
//fetch("https://treinamentoajax.herokuapp.com/messages/19", {
//    method:"DELETE"
//}).then(console.log)

//PUT/PATCH

// let fetchBody={
//     "message":{
//         "message":"alteração do teste-2"
//     }
// }

// let fetchConfig ={
//     method:"PUT",
//     headers:{"Content-Type": "application/JSON"},
//     body: JSON.stringify(fetchBody)
// }

// fetch("https://treinamentoajax.herokuapp.com/messages/42", fetchConfig)
// .then(response => response.json())
// .then(console.log)
// .catch()